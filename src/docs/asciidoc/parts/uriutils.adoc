== URI Utilities

=== Converting objects to URIs

Use the link:{groovydoc}++/api/UriUtils.html#urize(java.lang.Object)++[urize] method to convert nearly anything to a URI. If objects that have `toURI()` methods those methods will be called, otherwise objects that are convertible to strings, will effectively call `toString().toURI()`. Closures will be evaluated and the results are then converted to URIs.

[source,groovy]
----
include::{compatdir}/UriUtilsSpec.groovy[tags=urize,indent=0]
----
