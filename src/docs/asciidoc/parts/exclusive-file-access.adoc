== Exclusive File Access

When creating a plugin that will potentially access shared state between different gradle projects, such as downlaoded files, co-operative exclusive file access. This can be achieved by using {groovydoc}/api/ExclusiveFileAccess.html.

[source,groovy]
----
File someFile

ExclusiveFileAccess accessManager = new ExclusiveFileAccess(120000, 200) // <1>

accessManager.access( someFile ) {
  // ... do something, whilst someFile is being accessed
} // <2>
----
<1> Set the timeout waiting for file to become available and the poll frequency. Both are in milliseconds.
<2> Run this closure whislt this file is locked. You can also use anything that implements `Callable<T>`.

The value returned from the closure or callable is the one returned from the `access` method.